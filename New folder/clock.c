//DONG HO SU DUNG TIMER    BLOG DIEN TU - TIN HOC EDIT By ChipKool.net
//PHAN CUNG: 89c51;    THACH ANH: 12MHZ ;   
//SU DUNG 6 LED 7 DOAN GIO : PHUT : GIAY
 
/************KHAI BAO THU VIEN, DINH NGHIA********************************************* ***********/
#include<AT89X52.H>
 
#define Set_key P3_4
#define Up_key    P3_5
#define Dw_key    P3_6
#define SDA P1_1   
#define SCL P1_0   
#define DS1307_ID 0xD0   
#define SEC 0x00
#define    MIN 0x01   
#define HOUR 0x02
//#define DATE 0x04
//#define MONTH 0x05
//#define YEAR 0x06
 
/************KHAI BAO BIEN, MANG...******************************************* ********************/
unsigned char led7_data[10] = {0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90 }; //Decimal to Led7seg
unsigned char t_view,time,index,F_set,B_nhay,get_t ;
unsigned char hour,sec,min;
 
/************KHAI BAO HAM, CHUONG TRINH CON*********************************************** ********/
void set_main(void);    // Chuong trinh khoi tao main
void setup_timer(void);    // Chuong trinh cai dat timer 1 va 0
void delay(unsigned int time); // Delay
void Fix_time(void);    // Kiem tra va hieu chinh gia tri cua gio,phut,giay
void Keypad(void);    // Kiem tra xu ly phim nhan.   
void T1_ISR(void);    //ngat timer 1 phuc vu nhay led
void T0_ISR(void); // Dung ngat timer 0 de quet led
 
/************CAC CHUONG TRINH CON*********************************************** ******************/
// FOR I2C
/************************************************** *********************/
 
void I2C_start(void)
{
SCL    = 1;    SCL = 0;
SDA = 1;    SCL = 1;
delay(2);
SDA = 0; delay(2);
SCL = 0; delay(2);
}
void I2C_stop(void)
{
SCL = 1;    SCL = 0;
SDA = 0;    delay(2);
SCL = 1;    delay(2);
SDA = 1;   
}
bit I2C_write(unsigned char dat)
{
unsigned char i;   
for (i=0;i<8;i++)
{
SDA = (dat & 0x80) ? 1:0;
SCL=1;SCL=0;
dat<<=1;
}
SCL = 1;    delay(2);
SCL = 0;
}
unsigned char I2C_read(void)
{
bit rd_bit;   
unsigned char i, dat;
dat = 0x00;   
for(i=0;i<8;i++)    /* For loop read data 1 byte */
{
delay(2);
SCL = 1;    /* Set SCL */
delay(2);
rd_bit = SDA;    /* Keep for check acknowledge    */
dat = dat<<1;   
dat = dat | rd_bit;    /* Keep bit data in dat */
SCL = 0;    /* Clear SCL */
}
return dat;
}
/************************************************** *********************/
// FOR DS1307
/************************************************** *********************/
 
unsigned char DS1307_get(unsigned char addr)
{
unsigned int temp,ret;   
I2C_start(); /* Start i2c bus */
I2C_write(DS1307_ID); /* Connect to DS1307 */
I2C_write(addr);    /* Request RAM address on DS1307 */   
I2C_start();    /* Start i2c bus */
I2C_write(DS1307_ID+1);    /* Connect to DS1307 for Read */
ret = I2C_read();    /* Receive data */
I2C_stop();
//*********************************************
temp = ret;    /*BCD to HEX*/
ret = (((ret/16)*10)+ (temp & 0x0f));    /*for Led 7seg*/
//*********************************************   
return ret;   
}
 
void DS1307_Write(unsigned char addr,unsigned char dat)
{
unsigned int temp;
//**********************************************    /*HEX to BCD*/
temp = dat ;    /*for Led 7seg*/
dat = (((dat/10)*16)|(temp %10));
//**********************************************   
I2C_start(); /* Start i2c bus */
I2C_write(DS1307_ID); /* Connect to DS1307 */
I2C_write(addr);    /* Request RAM address on DS1307 */   
I2C_write(dat);    /* Connect to DS1307 for Read */
I2C_stop();
}
/************************************************** *********************/
 
void delay(unsigned int time)
{    while(time--); }
void set_main(void)    // Chuong trinh khoi tao main
{
P1=0xFF;
P0=0x00;
P2=0x00;
P3=0xFF;
}
void setup_timer(void)    // Setup timer 0 va timer 1
{
TMOD=0x11; // timer0 & timer1 set che do 1
TH0=-1000/256; TL0=-1000%256;
TH1=0x3C; TL1=0xAF;
ET1=1; ET0=1;    EA=1;
TF0=0; TF1=0;    TR0=1;    TR1=1;
}
void Fix_time(void)    // Kiem tra va hieu chinh gia tri cua gio,phut,giay
{
//Tang
if(sec==60)    {sec=0;min++;    }
if(min==60)    {min=0;hour++;    }
if(hour==24) hour=0;
//Giam
if(sec== -1) {sec=59;min--;}
if(min== -1) {min=59;hour-- ;}
if(hour== -1)hour= 23;
}
void Keypad(void)    // Kiem tra phim nhan.   
{
if(!Set_key){    //phim Set duoc nhan ?
F_set++;    // Bien F_set co gia tri tu 0->2
if(F_set==4) {F_set=0;
DS1307_Write(0x02,hour);
DS1307_Write(0x01,min);
DS1307_Write(0x00,sec); }
}
if(F_set==1){    //Tang hoac giam phut neu F_set = 1
if(!Up_key) hour++;
if(!Dw_key)    hour--;
}
if(F_set==2){    //Tang hoac giam gio neu F_set = 2
if(!Up_key) min++;
if(!Dw_key) min--;
}
if(F_set==3){    //Tang hoac giam gio neu F_set = 2
if(!Up_key) sec++;
if(!Dw_key) sec--;
}
Fix_time();    //kiem tra tran so
delay(20000);
}
void T1_ISR(void) interrupt 3 //ngat timer 1 chay dong ho
{
TR1=0;
TF1=0;
TH1=0x3C; // nap lai gia tri cho thanh ghi
TL1=0xAF;
time++;   
if(time==10)
{time=0; B_nhay++; get_t=1;
if(B_nhay==2)
B_nhay=0;
}
TR1=1;
}   
/*************************************/
void T0_ISR(void) interrupt 1    // Dung ngat timer 0 de quet led
{
TR0=0;
TF0=0;
TH0=0xfc;//-1000/256; // Nap lai gia tri cho thanh ghi
TL0=0x18;//-1000%256;
index++;
//giay
if(index==1)   
{   
if(F_set==3 && B_nhay==1){index++;    index++;}
else
{
t_view=sec; //sec;
P2=0xff;
P0=0x20;//00100000
P2=led7_data[t_view%10];    }}    //lay so du sau khi chia 10
 
if(index==2)
{
P2=0xff;
P0=0x10;//00010000
P2=led7_data[t_view/10]; } //xuat gia tri hang chuc
//phut
if(index==3){
if(F_set==2 && B_nhay==1){index++;    index++;}
else
{    t_view=min;
P2=0xff;
P0=0x08;//00001000
P2=led7_data[t_view%10];    }}
 
if(index==4)
{ P2=0xff;
P0=0x04;//00000100
P2=led7_data[t_view/10];    }
//gio
if(index==5) {
if(F_set==1 && B_nhay==1){index=0;}
else
{ t_view=hour;
P2=0xff;
P0=0x02;//00000010
P2=led7_data[t_view%10];    } }
 
if(index==6)
{ P2=0xff;
P0=0x01;//00000001
P2=led7_data[t_view/10];
index=0;    }
TR0=1;   
}
/************CHUONG TRINH CHINH********************************************* **********************/
void main()
{
set_main();
time=index=0;
hour=0;
min=0;
sec=0;
F_set=0;
get_t=0;
setup_timer();
while(1)
{
Keypad();
 
if(F_set==0)
{
if(get_t == 1)
{   
get_t = 0;
sec = DS1307_get(SEC);
min = DS1307_get(MIN);   
hour = DS1307_get(HOUR);   
 
}
}
}
}